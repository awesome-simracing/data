# Awesome Simracing - Data

A curated list of awesome simracing data and resources.

## Track Data

Provides track data for (various) simracing games. The pattern accessing a single track information is
`https://data.awesome-simracing.com/track/<game>/<track>.json`. 

* `<game>` is the lowercase Simhub game id (`DataCorePlugin.CurrentGame`)
* `<track>` is the lowercase Simhub track id (`TrackId`)

### Games

  * [Assetto Corsa Competizione](public/game/assettocorsacompetizione)

## Kudos and honorable mentions

* Inital inspiration and data from [slemke's ACC Corner Names Overlay](https://www.racedepartment.com/downloads/live-track-corner-names-shows-all-corner-names-on-all-acc-tracks.38078/).
* [Simhub](https://www.simhubdash.com/)


## Contributions

Your contributions are always welcome! Please take a look at the [contribution guidelines](CONTRIBUTING.md) first.
